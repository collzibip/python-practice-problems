# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
     # num_attendees = the number of attendees
    num_attendees = len(attendees_list)                 # solution
    # num_members = the number of members
    num_members = len(members_list)

    if attendees_list >= members_list/2:
        return True
    else:
        return False

print(has_quorum(20,70))
